const express = require('express');
const router = express.Router();
const fs = require('fs');

router.get('/', function(req, res) {
  fs.readFile('./data.json', function(err, data) {
    if(err) throw err;
    let dateReturn = data.toString();
    res.render('list', { data: JSON.parse(dateReturn) });
  });
});
router.delete('/', function(req, res) {
  fs.readFile('./data.json', function(err, data) {
    if(err) throw err;
    let dateReturn = data.toString();
    let parse = JSON.parse(dateReturn);
    let newArr =  parse.filter((item) => {
      if (item.id !== +req.body.id)
        return item;
    });
    fs.writeFile('./data.json', JSON.stringify(newArr),()=>{
      res.send('ok');
    })
  });
});


module.exports = router;
