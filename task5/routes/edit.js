var express = require('express');
var router = express.Router();
const fs = require('fs');

router.get('/', function(req, res) {
    res.render('edit', { title: 'Login' });
});
router.post('/', function(req, res) {
    fs.readFile('./data.json', function(err, data) {
        if(err) throw err;
        let dateReturn = data.toString();
        let parse = JSON.parse(dateReturn);
        parse.push(req.body);
        fs.writeFile('./data.json', JSON.stringify(parse),()=>{
            res.send('ok');
        })
    });
});
router.put('/', function(req, res) {
    fs.readFile('./data.json', function(err, data) {
        if(err) throw err;
        let dateReturn = data.toString();
        let parse = JSON.parse(dateReturn);
        let newArr =  parse.map((item) => {
            if (item.id === req.body.id) {
                return req.body
            } else return item
        })
        fs.writeFile('./data.json', JSON.stringify(newArr),()=>{
            res.send('ok');
        })
    });
});


module.exports = router;