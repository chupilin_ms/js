var express = require('express');
var router = express.Router();
const fs = require('fs');

router.get('/', function(req, res) {
  res.render('index', { title: 'Login' });
});
router.post('/', function(req, res) {
  res.redirect('/list')
});

module.exports = router;
