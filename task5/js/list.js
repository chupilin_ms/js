const create = document.querySelector('.create');

function edit(evt) {
    localStorage['target'] = evt.target.name;
    window.location.href = 'http://localhost:3000/edit'
}

function del(evt) {
    let targetElem = document.getElementById(evt.target.name);
    $.ajax({
        url: "/list",
        contentType: "application/json",
        method: "DELETE",
        data: JSON.stringify({
            id: evt.target.name,
        }),
        success: function (message) {
            if(message === 'ok')
                targetElem.remove();
        }
    });
}

create.addEventListener('click', (evt) => {
    localStorage.clear();
    window.location.href = 'http://localhost:3000/edit'
});

// const edit = document.querySelectorAll('.edit');
// const del = document.querySelectorAll('.delete');
// for(var i=0;i<edit.length;i++) {
//     edit[i].addEventListener("click", (evt)=>{
//         localStorage['target'] = evt.target.name;
//         window.location.href = 'http://localhost:3000/edit'
//     });
//     del[i].addEventListener("click", (evt)=>{
//         let targetElem = document.getElementById(evt.target.name);
//         $.ajax({
//             url: "/list",
//             contentType: "application/json",
//             method: "DELETE",
//             data: JSON.stringify({
//                 id: evt.target.name,
//             }),
//             success: function (message) {
//                 if(message === 'ok')
//                     targetElem.remove();
//             }
//         });
//     });
// }



