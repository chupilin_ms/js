const creatOrEdit = document.querySelector('.creatOrEdit');
const firstnm = document.querySelector('.firstnm');
const lasttnm = document.querySelector('.lasttnm');
const ages = document.querySelector('.ages');
const emails = document.querySelector('.emails');

if (localStorage.target) {
    let data = JSON.parse(localStorage.target);
    firstnm.value = data.firstname;
    lasttnm.value = data.lastname;
    ages.value = data.age;
    emails.value = data.email;
}

    creatOrEdit.addEventListener('click', (evt) => {
    const checkEmail = /^\w+@\w+\.\w+$/;
    if (firstnm.value&&lasttnm.value&&ages.value&&emails.value&&checkEmail.test(emails.value)) {
        if (localStorage.target === undefined) {
            $.ajax({
                url: "/edit",
                contentType: "application/json",
                method: "POST",
                data: JSON.stringify({
                    id: Math.floor(Math.random() * (900000000 - 100000000 + 1)) + 100000000,
                    firstname: firstnm.value,
                    lastname: lasttnm.value,
                    age: ages.value,
                    email: emails.value,
                }),
                success: function (message) {
                    if(message === 'ok')
                    localStorage.clear();
                    window.location.href = 'http://localhost:3000/list'
                }
            });
        }  else {
            let data = JSON.parse(localStorage.target);
            $.ajax({
                url: "/edit",
                contentType: "application/json",
                method: "PUT",
                data: JSON.stringify({
                    id: data.id,
                    firstname: firstnm.value,
                    lastname: lasttnm.value,
                    age: ages.value,
                    email: emails.value,
                }),
                success: function (message) {
                    if(message === 'ok')
                    localStorage.clear();
                    window.location.href = 'http://localhost:3000/list'
                }
            });
        }
    }   else {
        console.log('error invalid value');
    }
});