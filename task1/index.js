let str1 = "+ 375 (29) 99-25-777";
let str2 = "8(029) 77-77-688";
let str3 = "41 (888) 9999-99-9";
let str4 = "+ 7 (777)-88-93 c v";
let str5 = "r.zubel.vironit@vironit.ru";
let str6 = "r.zubel@vironit.com";

let re1 = /^\+\s375\s\(\d{2}\)\s\d{2}-\d{2}-\d{3}$/;
let re2 = /^8\(0\d{2}\)\s\d{2}-\d{2}-\d{3}$/;
let re3 = /^41\s\(\d{3}\)\s\d{4}-\d{2}-\d{1}$/;
let re4 = /^\+\s7\s\(\d{3}\)\W\d{2}\W\d{2}\sc\s\w{1}$/;
let re5 = /^\w+\.\w+\.vironit@\w+\.\w+$/;
let re6 = /^\w+\.\w+@vironit\.\w+$/;

console.log(re1.test(str1));
console.log(re2.test(str2));
console.log(re3.test(str3));
console.log(re4.test(str4));
console.log(re5.test(str5));
console.log(re6.test(str6));