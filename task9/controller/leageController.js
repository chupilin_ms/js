const leageSevices = require('../services/leageSevices');


class LeageController {
    getAllDataWithStage (req, res)  {
        leageSevices.getAllDataWithStage()
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    getAllData (req, res)  {
        leageSevices.getAllData()
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    deleteUser (req, res)  {
        leageSevices.deleteUser(req.params.id)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    saveNewUser (req, res)  {
        leageSevices.saveNewUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    updateUser (req, res)  {
        leageSevices.updateUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
}

module.exports = new LeageController();