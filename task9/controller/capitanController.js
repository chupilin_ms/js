const capitanServices = require('../services/capitanServices');


class CapitanController {
    getAllData (req, res)  {
        capitanServices.getAllData()
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    deleteUser (req, res)  {
        capitanServices.deleteUser(req.params.id)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    saveNewUser (req, res)  {
        capitanServices.saveNewUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    updateUser (req, res)  {
        capitanServices.updateUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
}

module.exports = new CapitanController();