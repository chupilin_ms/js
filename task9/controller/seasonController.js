const seasonServices = require('../services/seasonServices');


class SeasonController {
    getAllDataWithLeage (req, res)  {
        seasonServices.getAllDataWithLeage()
            .then(result => {
                res.send(result)
            })
            .catch(error => res.status(error).send('error'))
    }
    caskadeDelete (req, res)  {
        seasonServices.caskadeDelete(req.params.id)
            .then(result => {
                res.send(result)
            })
            .catch(error => res.status(error).send('error'))
    }
    getAllData (req, res)  {
        seasonServices.getAllData()
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    deleteUser (req, res)  {
        seasonServices.deleteUser(req.params.id)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    saveNewUser (req, res)  {
        seasonServices.saveNewUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    updateUser (req, res)  {
        seasonServices.updateUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
}

module.exports = new SeasonController();
