const stageServices = require('../services/stageServices');


class StageController {
    getAllData (req, res)  {
        stageServices.getAllData()
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    deleteUser (req, res)  {
        stageServices.deleteUser(req.params.id)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    saveNewUser (req, res)  {
        stageServices.saveNewUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    updateUser (req, res)  {
        stageServices.updateUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
}

module.exports = new StageController();