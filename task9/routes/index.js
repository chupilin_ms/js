const express = require('express');
const router = express.Router();

router.get('/', function(req, res) {
  res.send('render index page');
});
router.post('/', function(req, res) {
  res.redirect('/list')
});

module.exports = router;
