const express = require('express');
const router = express.Router();
const controller = require('../controller/seasonController');
const leageController = require('../controller/leageController');
const stageController = require('../controller/stageController');
const raceController = require('../controller/raceController');
const capitanController = require('../controller/capitanController');
const teamController = require('../controller/teamController');

router // geta allData
    .get('/seasonWithLeages', controller.getAllDataWithLeage);
router // caskade delete
    .get('/caskadeDelete/:id', controller.caskadeDelete);
//season
router
    .get('/season', controller.getAllData)
    .delete('/season/:id', controller.deleteUser)
    .post('/season', function(req, res) {
        controller.saveNewUser(req, res);
    })
    .put('/season', function(req, res) {
        controller.updateUser(req, res);
    });
//season
router // geta leage+stage info
    .get('/leagesWithStage', leageController.getAllDataWithStage);
//leage
router
    .get('/leage', leageController.getAllData)
    .delete('/leage/:id', leageController.deleteUser)
    .post('/leage', function(req, res) {
        leageController.saveNewUser(req, res);
    })
    .put('/leage', function(req, res) {
        leageController.updateUser(req, res);
    });
//leage
//stage
router
    .get('/stage', stageController.getAllData)
    .delete('/stage/:id', stageController.deleteUser)
    .post('/stage', function(req, res) {
        stageController.saveNewUser(req, res);
    })
    .put('/stage', function(req, res) {
        stageController.updateUser(req, res);
    });
//stage
//races
router
    .get('/race', raceController.getAllData)
    .delete('/race/:id', raceController.deleteUser)
    .post('/race', function(req, res) {
        raceController.saveNewUser(req, res);
    })
    .put('/race', function(req, res) {
        raceController.updateUser(req, res);
    });
//races
//team
router
    .get('/team', teamController.getAllData)
    .delete('/team/:id', teamController.deleteUser)
    .post('/team', function(req, res) {
        teamController.saveNewUser(req, res);
    })
    .put('/team', function(req, res) {
        teamController.updateUser(req, res);
    });
//team
//capitan
router
    .get('/capitan', capitanController.getAllData)
    .delete('/capitan/:id', capitanController.deleteUser)
    .post('/capitan', function(req, res) {
        capitanController.saveNewUser(req, res);
    })
    .put('/capitan', function(req, res) {
        capitanController.updateUser(req, res);
    });
//capitan
module.exports = router;
