const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const leageScheme = new Schema({
    idSeason: {type: mongoose.Schema.Types.ObjectId, required: true},
    titleLeague: {type: String, required: true},
});

const Leages = mongoose.model("leage", leageScheme);
module.exports = Leages;

// {
//     "idSeason": "5c90a9c33743272168cf3158",
//         "titleLeague": "season 2"
// }