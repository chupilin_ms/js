const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const raceScheme = new Schema({
    idStage: {type: mongoose.Schema.Types.ObjectId, required: true},
    titleRace: {type: String, required: true},
    result: [
        {
            id_team: {type: mongoose.Schema.Types.ObjectId, required: true},
            team: String,
            result: String
        }
    ]
});

const Races = mongoose.model("races", raceScheme);
module.exports = Races;
