const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const teamScheme = new Schema({
    idCapitan: {type: mongoose.Schema.Types.ObjectId, required: true},
    team: {type: String, required: true}
});

const Team = mongoose.model("teams", teamScheme);
module.exports = Team;