const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const capitanScheme = new Schema({
    id_team: {type: mongoose.Schema.Types.ObjectId, required: true},
    capitan: {
        name: {type: String, required: true},
        age: {type: Number, required: true}
    }
});

const Capitan = mongoose.model("capitans", capitanScheme);
module.exports = Capitan;