const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const stageScheme = new Schema({
    idLeage: {type: mongoose.Schema.Types.ObjectId, required: true},
    titleStage: {type: String, required: true},
});

const Stages = mongoose.model("stages", stageScheme);
module.exports = Stages;

