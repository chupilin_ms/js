const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const seasonScheme = new Schema({
    titleSeason: {type: String, required: true},
    start: String,
    end: String,
});

const Seasons = mongoose.model("season", seasonScheme);
module.exports = Seasons;


// {
//     "titleSeason": "season 1",
//     "start": "20.01.2015",
//     "end": "20.01.2016"
// }