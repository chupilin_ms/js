const Teams = require('../mongo/model/teamSchema');

class Team {
    async getAllData() {
        return Teams.find();
    };
    async deleteUser(id) {
        return Teams.remove({id:+id});
    };
    async saveNewUser(req) {
        let teams = new Teams(req.body);
        return teams.save();
    };
    async updateUser(req) {
        return Teams.updateOne({_id: req.body._id}, req.body);
    };
}

module.exports = new Team();