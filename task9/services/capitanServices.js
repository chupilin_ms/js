const Сapitans = require('../mongo/model/capitanSchema');

class Сapitan {
    async getAllData() {
        return Сapitans.find();
    };
    async deleteUser(id) {
        return Сapitans.remove({id:+id});
    };
    async saveNewUser(req) {
        let capitans = new Сapitans(req.body);
        return capitans.save();
    };
    async updateUser(req) {
        return Сapitans.updateOne({_id: req.body._id}, req.body);
    };
}

module.exports = new Сapitan();