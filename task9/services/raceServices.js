const Races = require('../mongo/model/raceScheme');

class Race {
    async getAllData() {
        return Races.find();
    };
    async deleteUser(id) {
        return Races.remove({id:+id});
    };
    async saveNewUser(req) {
        let races = new Races(req.body);
        return races.save();
    };
    async updateUser(req) {
        return Races.updateOne({_id: req.body._id}, req.body);
    };
}

module.exports = new Race();