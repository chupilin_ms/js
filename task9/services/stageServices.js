const Stages = require('../mongo/model/stageScheme');

class Stage {
    async getAllData() {
        return Stages.find();
    };
    async deleteUser(id) {
        return Stages.remove({id:+id});
    };
    async saveNewUser(req) {
        let stages = new Stages(req.body);
        return stages.save();
    };
    async updateUser(req) {
        return Stages.updateOne({_id: req.body._id}, req.body);
    };
}

module.exports = new Stage();