const Leages = require('../mongo/model/leageScheme');
const mongoose = require("mongoose");

class Leage {
    async getAllDataWithStage() {
        return Leages.aggregate([
            {
                $lookup:
                    {
                        from: "stages",
                        localField: "_id",
                        foreignField: "idLeage",
                        as: "Stages"
                    },

            },
            { $match: { _id: { $eq: mongoose.Types.ObjectId("5c90ef12d605760a14a8f795") } }}
        ]);
    };
    async getAllData() {
        return Leages.find();
    };
    async deleteUser(id) {
        return Leages.remove({id:+id});
    };
    async saveNewUser(req) {
        let leages = new Leages(req.body);
        return leages.save();
    };
    async updateUser(req) {
        return Leages.updateOne({_id: req.body._id}, req.body);
    };
}

module.exports = new Leage();