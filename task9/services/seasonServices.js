const Seasons = require('../mongo/model/schemaSeason');
const Leages = require('../mongo/model/leageScheme');
const Stages = require('../mongo/model/stageScheme');
const Races = require('../mongo/model/raceScheme');
const mongoose = require("mongoose");

class SeasonServices {
    async getAllDataWithLeage() {
        return Seasons.aggregate([
            {
                $lookup:
                    {
                        from: "leages",
                        localField: "_id",
                        foreignField: "idSeason",
                        as: "Leages"
                    }
            },
            {
                $unwind: {
                    path: "$Leages",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup:
                    {
                        from: "stages",
                        localField: "Leages._id",
                        foreignField: "idLeage",
                        as: "Leages.Stages"
                    }
            },
            {
                $unwind: {
                    path: "$Leages.Stages",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup:
                    {
                        from: "races",
                        localField: "Leages.Stages._id",
                        foreignField: "idStage",
                        as: "Leages.Stages.Races"
                    }
            },
            {
                $unwind: {
                    path: "$Leages.Stages.Races",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $match:
                    {
                        'Leages.Stages.Races._id': { $eq: mongoose.Types.ObjectId("5c920b6192d09f2818e250b2") }
                    }
            },
        ]);
    };
    async caskadeDelete(id) {
         let data = await Seasons.aggregate([
            {
                $lookup:
                    {
                        from: "leages",
                        localField: "_id",
                        foreignField: "idSeason",
                        as: "Leages"
                    }
            },
            {
                $match:
                    {
                        _id: { $eq: mongoose.Types.ObjectId(id) }
                    }
            },
            {
                $unwind: {
                    path: "$Leages",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup:
                    {
                        from: "stages",
                        localField: "Leages._id",
                        foreignField: "idLeage",
                        as: "Leages.Stages"
                    }
            },
            {
                $unwind: {
                    path: "$Leages.Stages",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup:
                    {
                        from: "races",
                        localField: "Leages.Stages._id",
                        foreignField: "idStage",
                        as: "Leages.Stages.Races"
                    }
            },
            {
                $unwind: {
                    path: "$Leages.Stages.Races",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: '$_id',
                    Seasons: {
                        $addToSet : '$_id'
                    },
                    Leages: {
                        $addToSet : '$Leages._id'
                    },
                    Stages: {
                        $addToSet : '$Leages.Stages._id'
                    },
                    Races: {
                        $addToSet : '$Leages.Stages.Races._id'
                    }
                }
            },
        ]);
        await Seasons.remove({'_id': {$in: data[0].Seasons}});
        await Leages.remove({'_id': {$in: data[0].Leages}});
        await Stages.remove({'_id': {$in: data[0].Stages}});
        await Races.remove({'_id': {$in: data[0].Races}});
        return data;
    };
    async getAllData() {
        return Seasons.find();
    };
    async deleteUser(id) {
        return Seasons.remove({id:+id});
    };
    async saveNewUser(req) {
        let season = new Seasons(req.body);
        return season.save();
    };
    async updateUser(req) {
        return Seasons.updateOne({_id: req.body._id}, req.body);
    };
}

module.exports = new SeasonServices();
