const express = require('express');
const router = express.Router();
const controller = require('../controller/controller');

const checkEmail = /^\w+@\w+\.\w+$/;

// list
router
    .get('/list', controller.getAllData)
    .delete('/list', controller.deleteUser);
// edit
router
    .get('/edit', function(req, res) {
        res.render('edit');
    })
    .post('/edit', function(req, res) {
        if (req.body.id&&req.body.firstname&&req.body.lastname&&req.body.age&&req.body.email&&checkEmail.test(req.body.email)) {
            controller.saveNewUser(req, res);
        } else res.send('error');
    })
    .put('/edit', function(req, res) {
        if (req.body.id&&req.body.firstname&&req.body.lastname&&req.body.age&&req.body.email&&checkEmail.test(req.body.email)) {
            controller.updateUser(req, res);
        } else res.send('error');
    });

module.exports = router;
