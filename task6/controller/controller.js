const service = require('../services/services');


class Controller {
    getAllData (req, res)  {
        service.getAllData()
            .then(result => res.render('list', { data: result }))
    }
    deleteUser (req, res)  {
        service.deleteUser(req.body.id)
            .then(res.send('ok'))
    }
    saveNewUser (req, res)  {
        service.saveNewUser(req)
            .then(res.send('ok'))
    }
    updateUser (req, res)  {
        service.updateUser(req)
            .then(res.send('ok'))
    }
}

module.exports = new Controller();
