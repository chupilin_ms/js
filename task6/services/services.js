const User = require('../mongo/model/schemaUsers');


class Services {
    async getAllData() {
        return User.find();
    };
    async deleteUser(id) {
        return User.remove({id:+id});
    };
    async saveNewUser(req) {
        let user = new User(req.body);
        return user.save();
    };
    async updateUser(req) {
        return User.updateOne({id: req.body.id}, req.body);
    };
}

module.exports = new Services();

