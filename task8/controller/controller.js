const service = require('../services/services');


class Controller {
    getAllData (req, res)  {
        service.getAllData()
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    deleteUser (req, res)  {
        service.deleteUser(req.params.id)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    saveNewUser (req, res)  {
        service.saveNewUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
    updateUser (req, res)  {
        service.updateUser(req)
            .then(result => res.send(result))
            .catch(error => res.status(error).send('error'))
    }
}

module.exports = new Controller();
