const mongoose = require("mongoose");

const connectDb = function() {
    mongoose.connect("mongodb://localhost:27017/formula", { useNewUrlParser: true });
};

module.exports = connectDb;