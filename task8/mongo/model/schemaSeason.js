const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const seasonScheme = new Schema({
    titleSeason: String,
    League: [
        {
            titleLeague: String,
            Stage: [
                {
                    titleNumber: String,
                    Race: [
                        {
                            title: String,
                            Result: [
                                {
                                    id_team: Number,
                                    team: String,
                                    result: String
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});

// const team = new Schema({
//     idTeam: Number,
//     titleTeam: String,
//     user: {
//         login: String,
//         password: String,
//         email: String,
//     }
// });

const Seasons = mongoose.model("seasons", seasonScheme);
module.exports = Seasons;
