const express = require('express');
const router = express.Router();
const controller = require('../controller/controller');


router
/*
@oas [get] /list
description: "get all users"
tags:
  - list
summary: "get all users from db"
responses:
  200:
    description: OK
    content:
        application/json:
            schema:
                type: array
                items:
                    type: object
                    properties:
                        _id:
                            type: string
                            example: "343434343434343"
                        id:
                            type: number
                            example: 232323232
                        firstname:
                            type: string
                            example: max
                        lastname:
                            type: string
                            example: vasilun
                        age:
                            type: number
                            example: 22
                        email:
                            type: string
                            example: ss@ssw.ru
                        __v:
                            type: number
                            example: 0
  500:
    description: Error
*/
    .get('/list', controller.getAllData)
/*
@oas [delete] /list/{id}
description: "delete user by Id"
tags:
    - list
parameters:
    - (path) id=2323232323* {String} id
summary: "delete user from db"
responses:
  200:
    description: OK
  500:
    description: Error
*/
    .delete('/list/:id', controller.deleteUser);
// edit
router
/*
@oas [post] /edit
description: "create new user"
tags:
  - edit
requestBody:
    required: true
    content:
        application/json:
            schema:
               type: object
               properties:
                   id:
                       type: number
                       example: 232323232
                   firstname:
                       type: string
                       example: max
                   lastname:
                       type: string
                       example: vasilun
                   age:
                       type: number
                       example: 22
                   email:
                       type: string
                       example: ss@ssw.ru
summary: "create new user and save in db"
responses:
  200:
    description: OK
  406:
    description: error with value form
  500:
    description: Error
*/
    .post('/edit', function(req, res) {
            controller.saveNewUser(req, res);
    })
/*
@oas [put] /edit
description: "update user"
tags:
  - edit
requestBody:
    required: true
    content:
        application/json:
            schema:
               type: object
               properties:
                   id:
                       type: number
                       example: 232323232
                   firstname:
                       type: string
                       example: max
                   lastname:
                       type: string
                       example: vasilun
                   age:
                       type: number
                       example: 22
                   email:
                       type: string
                       example: ss@ssw.ru
summary: "update user and save in db"
responses:
  200:
    description: OK
  406:
    description: error with value form
  500:
    description: Error
*/
    .put('/edit', function(req, res) {
        if (req.body.id&&req.body.firstname&&req.body.lastname&&req.body.age&&req.body.email&&checkEmail.test(req.body.email)) {
            controller.updateUser(req, res);
        } else res.status(406).send('error with value');
    });

module.exports = router;
