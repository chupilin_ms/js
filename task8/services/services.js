const Seasons = require('../mongo/model/schemaSeason');

class Services {
    async getAllData() {
        return Seasons.find();
    };
    async deleteUser(id) {
        return Seasons.remove({id:+id});
    };
    async saveNewUser(req) {
        let season = new Seasons(req.body);
        return season.save();
    };
    async updateUser(req) {
        return Seasons.updateOne({id: req.body.id}, req.body);
    };
}

module.exports = new Services();
