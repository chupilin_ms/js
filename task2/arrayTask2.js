function findLargest(...rest) {
    return rest.map(item => Math.max(...item));
}

console.log(findLargest([1,25,4], [2,4,3], [43,2,1], [32, 43, 102]));
