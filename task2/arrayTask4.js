function findMissingLetter(str) {
    let array = str.split('').sort();
    let newArr = [];
    for (let i = 1; i < array.length; i++) {
        const prev = array[i - 1].charCodeAt();
        const current = array[i].charCodeAt();
        if (current - prev !== 1) {
            let number = current - prev - 1;
            for (let x = 1; x < number + 1; x++) {
               newArr.push(String.fromCharCode(prev + x));
            }
        }
    }
    return newArr.join(',');
}

console.log(findMissingLetter("badf"));

