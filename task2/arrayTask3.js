function findEl(arr, func) {
    return arr.find(item => func(item));
}

console.log(findEl([1, 5, 10, 19, 23, 30], function(num) { return num % 2 === 0; }));