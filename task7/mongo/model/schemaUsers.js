const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userScheme = new Schema({
    id: Number,
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    age: {
        type: Number,
        required: true,
    },
    email: {
        type: String,
        required: true,
    }
});
const User = mongoose.model("user", userScheme);
module.exports = User;