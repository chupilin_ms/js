const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const authScheme = new Schema({
    name: {
        type: String,
        required: true,
    },
    pass: {
        type: String,
        required: true,
    }
});
const Auth = mongoose.model("auth", authScheme);
module.exports = Auth;