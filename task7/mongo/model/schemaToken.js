const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const tokenScheme = new Schema({
    _id: {
        type: String,
        required: true,
    },
    token: {
        type: String,
        required: true,
    }
});
const Token = mongoose.model("token", tokenScheme);
module.exports = Token;