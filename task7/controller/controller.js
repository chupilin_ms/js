const service = require('../services/services');
const jwt = require('jsonwebtoken');
const passwordHash = require('password-hash');
const getTokenFromCookie = require('../modules/modules');

class Controller {
    getAllData (req, res)  {
        let checkToken = getTokenFromCookie.checkJWT(req).data;
        service.loginUser({_id: checkToken})
            .then(result => {return result})
            .then(data => {
                service.getAllData()
                    .then(result => res.render('list', { data: result, userName: `hello ${data[0].name}`}))
        })
    }
    deleteUser (req, res)  {
        service.deleteUser(req.body.id)
            .then(res.send('ok'))
    }
    saveNewUser (req, res)  {
        service.saveNewUser(req)
            .then(res.send('ok'))
    }
    updateUser (req, res)  {
        service.updateUser(req)
            .then(res.send('ok'))
    }
    loginUser (req, res)  {
        if (!req.body.firstname&&!req.body.Password) {
            res.status(400).send('error with data')
        }
        let {firstname: name, Password: pass} = req.body;
        let data = {
            name: name,
        };
        let token;
          service.loginUser(data)
            .then((result) => {
                if (result.length) {
                    if (passwordHash.verify (pass, result[0].pass)) {
                        return result;
                    } else {
                        res.redirect('/');
                    }
                } else {
                    res.redirect('/');
                }
            })
            .then((result)=>{
                const secret = 'dalbaeb';
                token = jwt.sign({
                    data: result[0]._id
                }, secret);
                let data = {
                    _id: result[0]._id,
                    token: token,
                };
                return service.saveNewToken(data)
            })
            .then((result) => {
                res.cookie('token', token, { maxAge: 300000, httpOnly: false});
                res.redirect('/list');
            })
    }
    saveNewAuthUser (req, res)  { //авторизация нового пользователя
        if (req.body.firstname&&req.body.Password) {
            let {firstname: name, Password: pass} = req.body;
            let hashedPassword = passwordHash.generate(pass);//шифрование данных
            let data = {
                name: name,
                pass: hashedPassword,
            };
            service.saveNewAuthUser(data)
                .then(res.redirect('/list'))
        }
    }
}

module.exports = new Controller();


