const User = require('../mongo/model/schemaUsers');
const Auth = require('../mongo/model/schemaAuth');
const Token = require('../mongo/model/schemaToken');


class Services {
    async getAllData() {
        return User.find();
    };
    async deleteUser(id) {
        return User.remove({id:+id});
    };
    async saveNewUser(req) {
        let user = new User(req.body);
        return user.save();
    };
    async updateUser(req) {
        return User.updateOne({id: req.body.id}, req.body);
    };
    // auth
    async loginUser(data) {
        return Auth.find(data);
    };
    async findByToken(data) {
        return Token.find(data);
    };
    async saveNewToken(data) {
        const token = new Token(data);
        const check = await this.findByToken({_id: data._id});
        if (check.length) {
            return Token.updateOne({_id: data._id}, data)
        } else {
            return token.save();
        }
    };
    // auth
    async saveNewAuthUser(data) { //авторизация нового пользователя
        let authUser = new Auth(data);
        return authUser.save();
    };
}

module.exports = new Services();

