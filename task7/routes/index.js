const express = require('express');
const router = express.Router();
const controller = require('../controller/controller');
const getTokenFromCookie = require('../modules/modules');

// router.post('/', function(req, res) {
//   if (req.body.token) {
//     res.send('ok');
//   } else {
//     res.redirect('/',{ title: 'Registration' })
//   }
// });

router.get('/', function(req, res) {
  let token = getTokenFromCookie.getTokenFromCookie('token', req);
  if (token) {
    res.redirect('/list');
  } else {
    res.render('index', { title: 'Registration' });
  }

});


router
    .post('/reqistration', controller.saveNewAuthUser)
    .post('/auth', controller.loginUser);

module.exports = router;
