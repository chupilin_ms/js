const create = document.querySelector('.create');

function edit(evt) {
    localStorage['target'] = evt.target.name;
    window.location.href = 'http://localhost:3000/edit'
}

function del(evt) {
    let targetElem = document.getElementById(evt.target.name);
    axios({
        method: 'DELETE',
        url: "/list",
        data: {
            id: evt.target.name,
        }
    })
        .then((result) => {
            if(result.data === 'ok')
                targetElem.remove();
        });
}

create.addEventListener('click', (evt) => {
    localStorage.clear();
    window.location.href = 'http://localhost:3000/edit'
});





