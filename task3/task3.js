function checkEnding1(str1, str2) {
    let string1 = str1.split('').splice(-str2.length).join('');
    return string1.toLowerCase() === str2.toLowerCase();
}

console.log(checkEnding1('Привет','ивет'));
console.log(checkEnding1('Ололошечка','хечка'));

function checkEnding2(str1, str2) {
    return str1.indexOf(str2) > -1
}

console.log(checkEnding2('Привет','ивет'));
console.log(checkEnding2('Привет','увет'));