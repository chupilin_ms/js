function func(arr1, arr2, index) {
    arr2Coppy = [...arr2];
    let lastItemsArr = arr2Coppy.splice(index);
    let startItmesArr = arr2.splice(0, index);
    startItmesArr.push(...arr1, ...lastItemsArr);
    return startItmesArr;
}

console.log(func([1, 2, 3], [4, 5, 6, 7], 3));